json.extract! user, :id, :name, :linkedin_count, :facebook_count, :twitter_count, :connection_score, :created_at, :updated_at
json.url user_url(user, format: :json)
