class User < ApplicationRecord
	after_save :update_connection_count
	
	SORTABLE_ATTRIBUTES = ["name", "linkedin_count", "facebook_count", "twitter_count", "connection_score"]

  def self.search_by(search_param)
  	if search_param.blank?
  		all
  	else
  		where("UPPER(name) LIKE UPPER(?)", "%#{search_param}%")
  	end
  end

  def self.order_by(order_by, order_direction)
  	if order_direction.present? && ["asc","ascending"].include?(order_direction.downcase)
  		direction = "ASC"
  	else
  		direction = "DESC"
  	end

  	if order_by.present? && SORTABLE_ATTRIBUTES.include?(order_by.downcase)
  		order_attribute = order_by.downcase
  	else
  		order_attribute = "connection_score"
  	end

  	order_query = order_attribute + " " + direction
  	order(order_query)
  end

	def update_connection_count
		total_count = self.linkedin_count + self.facebook_count + self.twitter_count
		case total_count
		when  0..50
			update_column(:connection_score, 1)
		when  51..100
			update_column(:connection_score, 1.5)
		when  101..200
			update_column(:connection_score, 5)
		when  201..400
			update_column(:connection_score, 10)
		else
			update_column(:connection_score, 15)
		end
	end
end
