class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.integer :linkedin_count
      t.integer :facebook_count
      t.integer :twitter_count
      t.float :connection_score

      t.timestamps
    end
  end
end
